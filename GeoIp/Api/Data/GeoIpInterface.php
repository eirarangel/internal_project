<?php

namespace Training\GeoIp\Api\Data;

interface GeoIpInterface extends \Magento\Framework\Api\CustomAttributesDataInterface 
{
	const ID = 'id';
	const IP = 'ip';
	const CITY = 'city';
	const MESSAGE = 'message';

	public function getId(){}
	public function setId($id){}

	public function getIp(){}
	public function setIp($ip){}


	public function getCity(){}
	public function setCity($city){}

	public function getMessage(){}
	public function setMessage($message){}

	public function getExtensionAttributes();

	public function setExtensionAttributes(\Magento\Customer\Api\Data\CustomerExtensionInterface $extensionAttributes);
}