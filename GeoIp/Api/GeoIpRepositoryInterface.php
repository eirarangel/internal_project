<?php

namespace Training\GeoIp\Api;

/**
 * Geo Ip interface.
 * @api
 */
interface GeoIpRepositoryInterface
{
    /**
     * Save Geo Ip.
     *
     * @param \Training\GeoIp\Api\Data\GeoIpInterface $geoIp
     * @return \Training\GeoIp\Api\Data\GeoIpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Training\GeoIp\Api\Data\GeoIpInterface $geoIp);

    /**
     * Retrieve geo ip.
     *
     * @param int $id
     * @return \Training\GeoIp\Api\Data\GeoIpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve geo ip matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Training\GeoIp\Api\Data\GeoIpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete geo ip.
     *
     * @param \Training\GeoIp\Api\Data\GeoIpInterface $geoIp
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Training\GeoIp\Api\Data\GeoIpInterface $geoIp);

    /**
     * Delete geo ip by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
