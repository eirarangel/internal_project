<?php
	
	namespace Training\GeoIp\Model\ResourceModel;

	use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

	class GeoIpData extends AbstractDb 
	{
		protected function _construct()
		{
			$this->_init('wombgeoip_data', 'id');
		}
	}