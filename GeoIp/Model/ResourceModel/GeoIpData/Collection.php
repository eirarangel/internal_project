<?php

	namespace Training\GeoIp\Model\ResourceModel\GeoIpData;

	use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

	class Collection extends AbstractCollection
	{

		protected function _construct()
		{
			$this->_init('Training\GeoIp\Model\GeoIpData', 'Training\GeoIp\Model\ResourceModel\GeoIpData');
		}
	}